#!/usr/bin/python3

from sys import argv
from factorial import factorial

def suma(m, n):
    return m + n

if __name__ == "__main__":
    if argv[1] == "factorial":
        # Ejecutar el factorial del 2 argumento, pasando a entero
        fact = factorial(int(argv[2]))
        print(fact)
    elif argv[1] == "suma":
        try:
            sumanumero = suma(int(argv[2]), int(argv[3]))
            print(sumanumero)
        except ValueError:
            print("Monstruo, escribe bien los argumentos")
        