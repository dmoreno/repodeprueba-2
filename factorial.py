#!/usr/bin/python3

from sys import argv


lista = ["a", "b", "c", 1]

dict = {
    "hola": "te saludo",
    "adios": "me despido"
}
def factorial(n):
    fact = 1
    while n > 1:
        fact = fact * n
        n = n - 1
    return fact


# MAIN EN PYTHON
if __name__ == "__main__":
    # Aqui va lo que se ejecuta primero
    for i in range(1,11):
        print(factorial(i))
